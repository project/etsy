# Etsy Shop Integration

[Etsy is the global marketplace](https://www.etsy.com) for unique and creative goods with millions of shops run by 
hobbiests, entrapenures, entrepreneurs, and others. This module was designed to allow shop owners to integrate their 
Etsy shop with their Drupal website.

This module suite has be rewritten from the ground up for Drupal 9 and 10 using v3 of the Etsy API and contains two main modules;
Etsy API and Etsy Shop. Other modules are encouraged to require either or both of these moudles as a dependency to 
implement shop features that you need for your site.

The Etsy API module provides a service wrapper to the Etsy API and is designed to integrate a single Etsy shop. The
Etsy Shop module is used as a base or example implementation for an Etsy Shop in Drupal. The Esty  Fields module contains 
the required fields that are specific to the Etsy Listing entities.

**No ecommerce capabilities are contained in either module to comply with the [Etsy API terms of service](https://www.etsy.com/legal/api/#use).**

For a full description of the module, visit the
[project page](https://www.drupal.org/project/etsy).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/etsy).

## Table of contents

- Prerequisites
- Requirements
- Installation
- Configuration
- Usage
- Known Issues
- Maintainers

## Prerequisites

You must register a new application with Etsy. To do this, go to the [Etsy developer portal](https://www.etsy.com/developers) 
and register a new application. Once you have created your application, you will be given your API keystring and secret.
You will need these values later when [configuring this module](#configuration).

## Requirements

This following community modules are required:

- [Imagecache External](https://www.drupal.org/project/imagecache_external) - The Etsy Shop module uses this to render images from external images hosted by Etsy.
- [Oauth2 Client](https://www.drupal.org/project/oauth2_client) - The Etsy API module uses this for authentication with the Etsy API.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

### OAuth2 Client

You will need to add your API credentials that you created under the [Prerequisites](#prerequisites) section above. To add your credentials,
navigate to _/admin/config/system/oauth2-client_. You will see an OAuth2 Client labeled "_etsy_". Under the Operations column,
click the arrow and select the _Edit_ link.

In the form, check the _Enabled_ checkbox. Then under the section labeled "_Client Settings: Etsy"_, enter your Etsy 
**keystring** into the _Client ID_ field and your Etsy **shared secret** into the _Client Secret_ field. Click the
button labeled _Save and request token_.

### Etsy API settings

To configure the module, navigate to _/admin/config/services/etsy_. Enter your Etsy shop ID in the field provided.

#### Caching

Currently Etsy API has a rate limit of requests to 5,000 per 24-hour period. Because of this, sites are encouraged to set a cache value
in the configuration. [See the official documentation](https://developers.etsy.com/documentation/essentials/rate-limits)
for more information on rate limits and how they apply to your use-case.

## Usage

### Etsy API

The Etsy API service is accessed like any other service as described in the [Drupal documentation](https://www.drupal.org/docs/drupal-apis/services-and-dependency-injection/services-and-dependency-injection-in-drupal-8#s-accessing-services)
using <em>dependency injection</em> or <em>static Drupal methods</em>.

### Etsy Shop

The Etsy Shop sub-module forms the base of displaying your Etsy shop in Drupal. It contains the Etsy Listing content
type as well as the Etsy Shop view, custom content display mode, and image styles.

### Etsy Fields

The Etsy Fields sub-module contains the following new fields that are specific to the Etsy Shop's Etsy Listing node type.

 * Etsy Price - Stores the listing price as received from the Etsy API which contains the amount, divisor, and currency code properties.

## Known Issues

No known issues. [Submit all issues to the project issue queue.](https://www.drupal.org/project/issues/etsy)

## Maintainers

Current maintainers:

- [r0nn1ef](https://www.drupal.org/r0nn1ef)

Original module (D6, D7) creators:

- [jdschroeder](https://www.drupal.org/u/jdschroeder)