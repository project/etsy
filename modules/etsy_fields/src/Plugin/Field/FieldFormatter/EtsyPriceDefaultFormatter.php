<?php

namespace Drupal\etsy_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'snippets_default' formatter.
 *
 * @FieldFormatter(
 *   id = "etsy_price_default",
 *   label = @Translation("Etsy price"),
 *   field_types = {
 *     "etsy_price"
 *   }
 * )
 */
class EtsyPriceDefaultFormatter extends FormatterBase {

  /**
   * @inheritDoc
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $supportedCurrencies = etsy_supported_currencies();

    foreach( $items as $delta => $item ) {
      $amount = $item->amount/$item->divisor;
      $source = [
        '#theme' => 'etsy_price',
        '#amount' => number_format($amount, 2, '.'),
        '#symbol' => $supportedCurrencies[$item->currency_code]['symbol'] ?? NULL,
        '#currency_code' => $item->currency_code,
      ];

      $elements[$delta] = ['#markup' => \Drupal::service('renderer')->render($source)];
    }

    return $elements;
  }

}