<?php

namespace Drupal\etsy_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the Etsy price field type.
 *
 * @FieldType(
 *   id = "etsy_price",
 *   label = @Translation("Etsy price"),
 *   description = @Translation("This field stores listing price in the database."),
 *   category = @Translation("Etsy"),
 *   default_widget = "etsy_price_default",
 *   default_formatter = "etsy_price_default"
 * )
 */
class EtsyPriceItem extends FieldItemBase {

  /**
   * @inheritDoc
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    return [
      'columns' => [
        'amount' => [
          'type' => 'int',
          'size' => 'normal',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ],
        'divisor' => [
          'type' => 'int',
          'size' => 'tiny',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 100,
        ],
        'currency_code' => [
          'type' => 'varchar',
          'length' => 5,
          'not null' => TRUE,
        ],
      ]
    ];
  }

  /**
   * @inheritDoc
   */
  public function isEmpty() {
    $value = $this->get('amount')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * @inheritDoc
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['amount'] = DataDefinition::create('integer')
      ->setLabel(t('Amount'));

    $properties['divisor'] = DataDefinition::create('integer')
      ->setLabel(t('Divisor'));

    $properties['currency_code'] = DataDefinition::create('string')
      ->setLabel(t('Currency code'));

    return $properties;
  }

}