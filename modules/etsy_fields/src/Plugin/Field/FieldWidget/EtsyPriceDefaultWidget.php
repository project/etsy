<?php

namespace Drupal\etsy_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'etsy_price_default' widget.
 *
 * @FieldWidget(
 *   id = "etsy_price_default",
 *   label = @Translation("Etsy price"),
 *   field_types = {
 *     "etsy_price"
 *   }
 * )
 */
class EtsyPriceDefaultWidget extends WidgetBase {

  /**
   * @inheritDoc
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['amount'] = [
      '#title' => $this->t('Amount'),
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->amount) ? $items[$delta]->amount : NULL,
    ];

    $element['divisor'] = [
      '#title' => $this->t('Divisor'),
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->divisor) ? $items[$delta]->divisor : NULL,
    ];

    $element['currency_code'] = [
      '#title' => $this->t('Currency code'),
      '#type' => 'select',
      '#default_value' => isset($items[$delta]->currency_code) ? $items[$delta]->currency_code : NULL,
      '#empty_option' => $this->t('Select a currency'),
      '#options' => $this->currencyCodeOptions(),
    ];

    return $element;
  }

  /**
   * Builds the currency code field options.
   *
   * @return array
   */
  protected function currencyCodeOptions() {
    $options = [];
    $supportedCurrencies = etsy_supported_currencies();
    foreach($supportedCurrencies as $key => $value ) {
      $options[$key] = sprintf('%s %s (%s)', $supportedCurrencies[$key]['symbol'], $supportedCurrencies[$key]['text'], $key);
    }
    return $options;
  }

}