<?php

namespace Drupal\etsy;

interface EtsyServiceInterface {

  /**
   * Ping the Etsy API to test that everything is set up correctly.
   *
   * @return false|mixed
   */
  public function ping(): mixed;

  /**
   * Returns basic info for the user making the request.
   *
   * @return false|mixed
   */
  public function getMe(): mixed;

  /**
   * Retrieves the shop object identified by a specific shop ID.
   *
   * @param null|string $key The property of the Shop object.
   *
   * @return false|mixed
   */
  public function shopInfo($key=NULL): mixed;

  /**
   * List Listings that belong to a Shop.
   *
   * @param int $shopId The unique positive non-zero numeric ID for an Etsy Shop.
   * @param int $limit The maximum number of results to return.
   * @param int $offset The number of records to skip before selecting the first result.
   * @param null|string $state Shop Listings that belong to a Shop. Listings can be filtered using the 'state' param.
   *
   * @return mixed array or boolean FALSE
   */
  public function getListingsByShop($shopId, $limit=25, $offset=0, $includes=[], $state=null ): mixed;

  /**
   * Retrieves a listing record by listing ID.
   *
   * @param int $listingId
   * @param array $includes
   *
   * @return false|mixed
   */
  public function getListingById(int $listingId, $includes=[]): mixed;

  /**
   * Get a listing's properties.
   *
   * @param int $listingId
   *
   * @return mixed
   */
  public function getListingProperties($listingId ): mixed;

  /**
   * Retrieves the list of shop sections in a configured shop.
   *
   * @return false|object
   */
  public function getShopSections(): object|bool;

}