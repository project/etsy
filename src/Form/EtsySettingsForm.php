<?php

namespace Drupal\etsy\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\etsy\EtsyService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EtsySettingsForm extends ConfigFormBase {

  /**
   * @var \Drupal\etsy\EtsyService
   */
  protected $etsyService;

  public function __construct(ConfigFactoryInterface $config_factory, EtsyService $service) {
    $this->etsyService = $service;
    parent::__construct($config_factory);
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('etsy.api')
    );
  }

  protected function getEditableConfigNames() {
    return ['etsy.settings'];
  }

  public function getFormId() {
    return 'etsy_settings_form';
  }

  function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('etsy.settings');

    $form['shop_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Etsy shop id'),
      '#description' => $this->t('Your Etsy shop id.'),
      '#required' => TRUE,
      '#default_value' => $config->get('shop_id'),
    ];

    $cache_options = [
      0 => $this->t('Disabled'),
      3600 => $this->t('1 hour'),
      7200 => $this->t('2 hours'),
      21600 => $this->t('6 hours'),
      43200 => $this->t('12 hours'),
      86400 => $this->t('24 hours'),
    ];
    $form['cache_lifetime'] = [
      '#type' => 'select',
      '#title' => $this->t('Cache lifetime'),
      '#description' => $this->t('The length of time to cache API results.'),
      '#options' => $cache_options,
      '#default_value' => $config->get('cache_lifetime')
    ];
    return parent::buildForm($form, $form_state); // TODO: Change the autogenerated stub
  }

  function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('etsy.settings')
      ->set('shop_id', $form_state->getValue('shop_id'))
      ->set('cache_lifetime', $form_state->getValue('cache_lifetime'))
      ->save();
  }

}