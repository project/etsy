<?php

namespace Drupal\etsy\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an Etsy trademark notice block with the text that is required by
 * the Etsy API Terms of Service.
 *
 * @Block(
 *   id = "etsy_trademark_block",
 *   admin_label = @Translation("Etsy trademark block")
 * )
 *
 * @see https://www.etsy.com/legal/api#marks
 */
class EtsyTrademarkNoticeBlock extends BlockBase {

  /**
   * @inheritDoc
   */
  public function build() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t("The term 'Etsy' is a trademark of Etsy, Inc. This application uses the Etsy API but is not endorsed or certified by Etsy.")
    ];
  }

}