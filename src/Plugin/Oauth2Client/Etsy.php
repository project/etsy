<?php

namespace Drupal\etsy\Plugin\Oauth2Client;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Drupal\oauth2_client\Plugin\Oauth2Client\Oauth2ClientPluginBase;
use Drupal\etsy\Provider\EtsyProvider;

/**
 * OAuth2 Client to authenticate with Esty.
 *
 * @Oauth2Client(
 *   id = "etsy",
 *   name = @Translation("Etsy"),
 *   grant_type = "authorization_code",
 *   authorization_uri = "https://www.etsy.com/oauth/connect",
 *   token_uri = "https://api.etsy.com/v3/public/oauth/token",
 *   resource_owner_uri = "",
 *   scopes = {
 *     "listings_d",
 *     "listings_r",
 *     "listings_w",
 *     "listings_d",
 *     "email_r",
 *     "shops_r",
 *     "shops_w",
 *     "profile_r",
 *     "profile_w"
 *   },
 *   scope_separator = " ",
 *   success_message = FALSE,
 *   request_options = {}
 * )
 */
class Etsy extends Oauth2ClientPluginBase {

  public function getRequestOptions(array $additionalOptions = []): array {
    $options = parent::getRequestOptions($additionalOptions);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function storeAccessToken(AccessTokenInterface $accessToken): void {
    $this->state->set('oauth2_client_access_token-' . $this->getId(), $accessToken);
    if ($this->displaySuccessMessage()) {
      $this->messenger->addStatus(
        $this->t('OAuth token stored.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveAccessToken(): ?AccessTokenInterface {
    return $this->state->get('oauth2_client_access_token-' . $this->getId());
  }

  /**
   * {@inheritdoc}
   */
  public function clearAccessToken(): void {
    $this->state->delete('oauth2_client_access_token-' . $this->getId());
  }

  /**
   * Creates a new provider object.
   *
   * @return \League\OAuth2\Client\Provider\GenericProvider
   *   The provider of the OAuth2 Server.
   */
  public function getProvider(): AbstractProvider {
    return new EtsyProvider(
      [
        'clientId' => $this->getClientId(),
        'clientSecret' => $this->getClientSecret(),
        'redirectUri' => $this->getRedirectUri(),
        'urlAuthorize' => $this->getAuthorizationUri(),
        'urlAccessToken' => $this->getTokenUri(),
        'urlResourceOwnerDetails' => $this->getResourceUri(),
        'scopes' => $this->getScopes(),
        'scopeSeparator' => $this->getScopeSeparator(),
        'pkceMethod' => 'S256',
        'pkceCode' => 'hXWzBkLFgx_rPotAHsctJAn15-EJ-My36A7mJepjYHuEov9gN6wYkckODtiCpyZp',
      ],
      $this->getCollaborators()
    );
  }

}